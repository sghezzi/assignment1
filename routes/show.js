var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const Item = require('../models/Item');

mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

/* GET home page. */
router.get('/', function(req, res, next) {
  Item.find()
    .then(items => res.render('show', { items }))
    .catch(err => res.status(404).json({ msg: 'No items found' }));
});

module.exports = router;