// Commento
var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const Cryptr = require('cryptr');
const cryptr = new Cryptr('jvwbrelbfbsKCBSFELEWFcbdebrr');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const User = require('../models/User');

mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/read', function(req, res, next) {
  res.redirect('/show');
});

router.post('/signup', (req, res) => {

  if (req.body.username && req.body.password) {
    var userData = ({
      username: req.body.username,
      password: req.body.password
    });

    var str1 = cryptr.encrypt(userData.username);
    var str2 = cryptr.encrypt(userData.password);
    var str3 = '/insert/?username=';
    var str4= '&password=';
   
    var stringResult = (str3.concat((str1)).concat(str4).concat((str2)));
    res.redirect(stringResult);
  }else {
    //res.redirect('/index');
    res.redirect('/');
  }


  
});

module.exports = router;
