var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const Cryptr = require('cryptr');
const cryptr = new Cryptr('jvwbrelbfbsKCBSFELEWFcbdebrr');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const Item = require('../models/Item');

var session;

var usernameVar;
var passwordVar;
var originalUsernameVar;
var originalPasswordVar;

mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

/* GET greatings. */
router.get('/', (req, res) => {

  //session = req.baseUrl;
  //console.log("SESSION:" + session);
  
  originalUsernameVar = req.query.username;
  originalPasswordVar = req.query.password;

  usernameVar = cryptr.decrypt((req.query.username));
  passwordVar = cryptr.decrypt((req.query.password));

  User.findOne({username: usernameVar, password: passwordVar}, function(err, user){
    console.log(user);
    if (user) {
      res.render('insert');
    }
    else {
      res.redirect('/');
    }
  });
});

/* POST */
router.post('/item/add', (req, res) => {
  const newItem = new Item({
    name: req.body.name
  });

  var str1 = '/insert/?username=';
  var str2 = originalUsernameVar;
  var str3 = '&password=';
  var str4 = originalPasswordVar;

  var resultString = str1.concat(str2).concat(str3).concat(str4);
  newItem.save().then(item => res.redirect(resultString));
});

router.get('/item/show', (req, res) => {
  var str1 = '/privateShow/?username=';
  var str2 = originalUsernameVar;
  var str3 = '&password=';
  var str4 = originalPasswordVar;

  var resultString = str1.concat(str2).concat(str3).concat(str4);
  res.redirect(resultString);
});

module.exports = router;