var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const Cryptr = require('cryptr');
const cryptr = new Cryptr('jvwbrelbfbsKCBSFELEWFcbdebrr');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const Item = require('../models/Item');
const User = require('../models/User');

var usernameVar;
var passwordVar;
var originalUsernameVar;
var originalPasswordVar;

mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

router.get('/', function(req, res, next) {

  originalUsernameVar = req.query.username;
  originalPasswordVar = req.query.password;

  usernameVar = cryptr.decrypt((req.query.username));
  passwordVar = cryptr.decrypt((req.query.password));

  console.log("Sono nel punto giusto");

  User.findOne({username: usernameVar, password: passwordVar}, function(err, user){
    if (user) {
        console.log("I'm in");
        Item.find()
        .then(items => res.render('privateShow', { items }))
        .catch(err => res.status(404).json({ msg: 'No items found' }));
    }
    else {
      console.log("I'm not in");
      res.redirect('/show');
    }
  });
});

router.post('/return', (req, res) => {

  var str1 = '/insert/?username=';
  var str2 = originalUsernameVar;
  var str3 = '&password=';
  var str4 = originalPasswordVar;

  var resultString = str1.concat(str2).concat(str3).concat(str4);
  res.redirect(resultString);
});


module.exports = router;