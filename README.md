## Note preliminari

Accertarsi di avere installato: Docker. Prima di eseguire lo script sotto descritto accertarsi di trovarsi nella directory contenete il progetto qui presentato.

## Containerization
$ ./docker-run.sh

## Rimozione di tutti i containers precedentemente "costruiti"
Si consiglia di eseguire SEMPRE il seguente script al fine di evitare conflitti fra containers "costruiti" in momenti del tempo differenti.
$ ./docker-stop.sh