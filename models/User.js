const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    password: {
      type: String,
      required: true,
    }
  });

module.exports = User = mongoose.model('user', UserSchema);

var admin1 = new User ({
  username: 'anto',
  password: 'anto'
});

var admin2 = new User ({
  username: 'simo',
  password: 'simo'
});

admin1.save();
admin2.save();
